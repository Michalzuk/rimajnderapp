package io.michalzuk.rimajnder;

import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import io.michalzuk.rimajnder.data.DatabaseData;
import io.michalzuk.rimajnder.data.DatabaseHelper;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private FloatingActionButton mAddReminderButton;
    private Toolbar mToolbar;
    ReminderCursorAdapter mCursorAdapter;
//    DatabaseHelper alarmReminderDbHelper = new DatabaseHelper(this);
    ListView reminderListView;

    private static final int VEHICLE_LOADER = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitle(R.string.app_name);


        reminderListView = findViewById(R.id.list);
        View emptyView = findViewById(R.id.empty_view);
        reminderListView.setEmptyView(emptyView);

        mCursorAdapter = new ReminderCursorAdapter(this, null);
        reminderListView.setAdapter(mCursorAdapter);

        reminderListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                Intent intent = new Intent(MainActivity.this, NewReminderActivity.class);

                Uri currentVehicleUri = ContentUris.withAppendedId(DatabaseData.AlarmReminderEntry.CONTENT_URI, id);


                intent.setData(currentVehicleUri);

                startActivity(intent);

            }
        });


        mAddReminderButton = findViewById(R.id.fab);

        mAddReminderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), NewReminderActivity.class);
                startActivity(intent);
            }
        });

        getLoaderManager().initLoader(VEHICLE_LOADER, null, this);


    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = {
                DatabaseData.AlarmReminderEntry._ID,
                DatabaseData.AlarmReminderEntry.KEY_TITLE,
                DatabaseData.AlarmReminderEntry.KEY_DATE,
                DatabaseData.AlarmReminderEntry.KEY_TIME,
                DatabaseData.AlarmReminderEntry.KEY_REPEAT,
                DatabaseData.AlarmReminderEntry.KEY_REPEAT_NO,
                DatabaseData.AlarmReminderEntry.KEY_REPEAT_TYPE,
                DatabaseData.AlarmReminderEntry.KEY_ACTIVE

        };

        return new CursorLoader(this,
                DatabaseData.AlarmReminderEntry.CONTENT_URI,
                projection,
                null,
                null,
                null);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mCursorAdapter.swapCursor(cursor);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCursorAdapter.swapCursor(null);

    }
}
