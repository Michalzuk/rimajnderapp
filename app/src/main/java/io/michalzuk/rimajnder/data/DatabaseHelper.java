package io.michalzuk.rimajnder.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;



/**
 * Created by michalzuk on 05/12/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "rimajnderDatabase.db";

    private static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String SQL_CREATE_ALARM_TABLE =  "CREATE TABLE " + DatabaseData.AlarmReminderEntry.TABLE_NAME + " ("
                + DatabaseData.AlarmReminderEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DatabaseData.AlarmReminderEntry.KEY_TITLE + " TEXT NOT NULL, "
                + DatabaseData.AlarmReminderEntry.KEY_DATE + " TEXT NOT NULL, "
                + DatabaseData.AlarmReminderEntry.KEY_TIME + " TEXT NOT NULL, "
                + DatabaseData.AlarmReminderEntry.KEY_REPEAT + " TEXT NOT NULL, "
                + DatabaseData.AlarmReminderEntry.KEY_REPEAT_NO + " TEXT NOT NULL, "
                + DatabaseData.AlarmReminderEntry.KEY_REPEAT_TYPE + " TEXT NOT NULL, "
                + DatabaseData.AlarmReminderEntry.KEY_ACTIVE + " TEXT NOT NULL " + " );";

        sqLiteDatabase.execSQL(SQL_CREATE_ALARM_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
